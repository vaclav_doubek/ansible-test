from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleError

class ActionModule(ActionBase):

    TRANSFERS_FILES = False
    
    def run(self, tmp=None, task_vars=None):

        if task_vars is None:
            task_vars = dict()

        result = super(ActionModule, self).run(tmp, task_vars)
        
        if "new_given_pass" in self._task.args:
            new_given_pass = self._task.args.get("new_given_pass")
            print("Current value:",self._loader._b_vault_password)
            print("New value:",new_given_pass)
            self._loader.set_vault_password(new_given_pass)
            print("Value set to:",self._loader._b_vault_password)
        else:
            result['failed'] = True
            result['msg'] = "No vault password specified. Except 'new_given_pass' param!"
            return result

        result['failed'] = False
        result['msg'] = "Password successfully set."

        return result